//
//  ListViewContoller.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit

protocol ListViewController: AnyObject {
    func reloadList()
}

class ListDefaultViewController: UIViewController, ListViewController {

    static func build() -> ListDefaultViewController {
        let viewController = UIStoryboard.main.instantiateViewController(of: ListDefaultViewController.self)!
        let router = ListDefaultRouter(viewController: viewController)
        let interactor = ListInteractor()

        viewController.presenter = ListPresenter(view: viewController, interactor: interactor, router: router)

        return viewController
    }

    private var presenter: ListPresenter!

    @IBOutlet weak var tableView: UITableView!
    
    private let CellIdentifier = "ListCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.viewWillAppear()
    }
    
    func reloadList() {
        tableView.reloadData()
    }
}

extension ListDefaultViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)!
        cell.textLabel?.text = presenter.listItem(at: indexPath).title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objectId = presenter.listItem(at: indexPath).id
        presenter.listItemSelected(objectId: objectId)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
